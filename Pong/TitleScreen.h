#pragma once

#include <iostream>
#include "GameScreen.h"

class TitleScreen : public GameScreen
{
public:
	TitleScreen();
	~TitleScreen();

	void LoadContent();
	void UnloadContent();
	void HandleInput(sf::Event event);
	void Update();
	void Draw(sf::RenderWindow &Window);

private:
	sf::Font font;
	sf::Text text;
};

