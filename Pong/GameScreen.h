#pragma once

#include<SFML/Graphics.hpp>
#include "InputManager.h"

class GameScreen
{
public:
	GameScreen();
	~GameScreen();

	virtual void LoadContent();
	virtual void UnloadContent();
	virtual void HandleInput(sf::Event event);
	virtual void Update();
	virtual void Draw(sf::RenderWindow &Window);

protected:
	InputManager input;
	std::vector<int> keys;
};

