#include "SplashScreen.h"



SplashScreen::SplashScreen()
{
}


SplashScreen::~SplashScreen()
{
}

void SplashScreen::LoadContent()
{
	if (!font.loadFromFile("arial.ttf")) {
		std::cout << "Could not find font arial.ttf" << std::endl;
	}
	text.setFont(font);
	text.setString("Hello World!");

	keys.push_back(sf::Keyboard::Z);
	keys.push_back(sf::Keyboard::Enter);
}

void SplashScreen::UnloadContent()
{
	GameScreen::UnloadContent();
}

void SplashScreen::HandleInput(sf::Event event)
{
	input.Update(event);
	if (input.KeyPressed(keys)) {

	}
}

void SplashScreen::Update()
{

}

void SplashScreen::Draw(sf::RenderWindow &Window)
{
	Window.draw(text);
}