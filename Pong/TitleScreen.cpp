#include "TitleScreen.h"



TitleScreen::TitleScreen()
{
}


TitleScreen::~TitleScreen()
{
}

void TitleScreen::LoadContent()
{
	if (!font.loadFromFile("arial.ttf")) {
		std::cout << "Could not load font arial.ttf" << std::endl;
	}
	text.setFont(font);
	text.setString("TitleScreen");
}

void TitleScreen::UnloadContent()
{

}

void TitleScreen::HandleInput(sf::Event event)
{
	input.Update(event);
}


void TitleScreen::Update()
{

}

void TitleScreen::Draw(sf::RenderWindow &Window)
{
	Window.draw(text);
}