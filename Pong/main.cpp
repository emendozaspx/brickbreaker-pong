#include <SFML/Graphics.hpp>
#include "ScreenManager.h"

int main()
{
	sf::RenderWindow Window(sf::VideoMode(640, 360, 32), "Breakout!");

	ScreenManager screenManager;
	screenManager.Initialize();
	screenManager.LoadContent();

	while (Window.isOpen())
	{
		sf::Event event;
		while (Window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed) {
				Window.close();
			}

			if (event.key.code == sf::Keyboard::S) {
				screenManager.AddScreen(new SplashScreen);
			}

			else if (event.key.code == sf::Keyboard::T) {
				screenManager.AddScreen(new TitleScreen);
			}
		}
		Window.clear();

		screenManager.Update();
		screenManager.Draw(Window);

		Window.display();
	}

	return 0;
}