#include "ScreenManager.h"



ScreenManager::ScreenManager()
{
}


ScreenManager::~ScreenManager()
{
}

void ScreenManager::AddScreen(GameScreen *Screen)
{
	currentScreen->UnloadContent();
	delete currentScreen;
	currentScreen = Screen;
	currentScreen->LoadContent();
}

void ScreenManager::Initialize()
{
	currentScreen = new SplashScreen();
}

void ScreenManager::LoadContent()
{
	currentScreen->LoadContent();
}

void ScreenManager::UnloadContent()
{
	currentScreen->UnloadContent();
}

void ScreenManager::Update()
{
	currentScreen->Update();
}

void ScreenManager::Draw(sf::RenderWindow &Window)
{
	currentScreen->Draw(Window);
}