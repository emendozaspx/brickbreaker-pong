#pragma once

#include <iostream>
#include "GameScreen.h"
#include "SplashScreen.h"
#include "TitleScreen.h"

class ScreenManager
{
public:
	ScreenManager();
	~ScreenManager();

	void Initialize();
	void LoadContent();
	void HandleInput(sf::Event event);
	void UnloadContent();
	void Update();
	void Draw(sf::RenderWindow &Window);

	void AddScreen(GameScreen *Screen);

private:
	GameScreen *currentScreen, *newScreen;
};

